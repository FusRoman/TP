#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <errno.h>
#include <string.h>
#include "utils.h"

#define PORT 9600
#define BUFFSIZE 1500;

int main(int argc, char *argv[])
{

    // Define descriptive strucutre of the local adress
    struct sockaddr_in serv_addr;
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = PORT;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    struct sockaddr *p_serv_addr = (struct sockaddr *)&serv_addr;
    int len_sock_addr = sizeof(serv_addr);

    // Pointer to the descriptive structure of the client address
    struct sockaddr *p_client_addr = malloc(sizeof(struct sockaddr));

    // Socket file description
    int sockfd = socket(PF_INET, SOCK_DGRAM, 0);

    // Bind the socket to the local socket
    int res_bind = bind(sockfd, p_serv_addr, len_sock_addr);
    printf("Result bind : %d\n", res_bind);
    if (res_bind < 0)
    {
        printf("%s\n", strerror(errno));
        return 1;
    }

    // Print the messages
    fflush(stdout);

    // Get length of the local adress
    socklen_t *len_addr = malloc(sizeof(socklen_t));

    // Initialize buffer
    char *buffer = (char *)malloc(sizeof(char) * 21);
    int nb_bytes_rcv = 1;
    int not_stop = 1;

    // Loop that waits msg from client
    while (not_stop && nb_bytes_rcv != -1)
    {
        // Get msg from the client and print this msg on the standard output
        int nb_bytes_rcv = recvfrom(sockfd, buffer, 21, 0, p_client_addr, len_addr);
        printf("Number of bytes received : %d\n", nb_bytes_rcv);
        printf("%s\n", buffer);
        not_stop = strcmp(buffer, STOP);
    }

    // Free all allocate memory
    free(p_client_addr);
    close(sockfd);
    free(buffer);
    free(len_addr);
    return 0;
}