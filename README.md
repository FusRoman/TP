Nom binôme 1 : NGUYEN THIET Lam

Nom binôme 2 : LE MONTAGNER Roman

# Commandes

Pour compiler :

```shell
> make
```

Pour la suite, il faudra compiler et se placer à la racine du dépôt.

Pour lancer le serveur udp :

```shell
> ./server_udp.out
```

Pour lancer le serveur tcp : 

```shell
> ./server_tcp.out
```

Pour lancer le client udp :

```shell
> ./client_udp.out <une adresse de la forme x.x.x.x>
```

Pour lancer le client tcp :

```shell
> ./client_tcp.out <une adresse de la forme x.x.x.x>
```

Pour supprimer les fichiers de bytecode :

```shell
> make clean
```

# Envoi de message

Il faut écrire une chaîne de 20 charactères, pour fermer la connexion, écrire ST0P. (le 0 est un zéro)
