#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#define STOP "ST0P\n"
char *concat(const char *s1, const char *s2);
void clear_buffer(char *buffer, int len);
int read_input(char *buffer, int len);