#include "utils.h"

// https://stackoverflow.com/questions/8465006/how-do-i-concatenate-two-strings-in-c/8465083
char *concat(const char *s1, const char *s2)
{
    char *result = malloc(strlen(s1) + strlen(s2) + 1); // +1 for the null-terminator
    strcpy(result, s1);
    strcat(result, s2);
    return result;
}

// clear the buffer ( set all the buffer to zero), does not check for seg fault
void clear_buffer(char *buffer, int len)
{
    for (int i = 0; i < len; i++)
    {
        buffer[i] = 0;
    }
}

// self explanatory, return the number of characters read written
int read_input(char *buffer, int len)
{
    // Initialize variables
    char c = 'x';
    int size = 0;

    // Read the input
    while (c != (int)'\n' && size <= 20)
    {
        c = getchar();
        buffer[size] = c;
        size++;
    }
    // end of string
    buffer[size] = 0;

    return size + 1;
}