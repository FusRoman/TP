#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <errno.h>
#include <string.h>
#include "utils.h"

#define PORT 9600
#define BUFFSIZE 1500;

int main(int argc, char *argv[])
{

    // Local address of the server
    struct sockaddr_in serv_addr;
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = PORT;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    struct sockaddr *p_serv_addr = (struct sockaddr *)&serv_addr;
    int len_sock_addr = sizeof(serv_addr);

    // Bind the socket to the server adress
    int sockfd = socket(PF_INET, SOCK_STREAM, 0);
    int res_bind = bind(sockfd, p_serv_addr, len_sock_addr);
    printf("Result bind : %d\n", res_bind);
    if (res_bind < 0)
    {
        printf("%s", strerror(errno));
        return 1;
    }

    // Listen to incoming requests
    int listen_res = listen(sockfd, 10);
    printf("Result listen : %d\n", listen_res);
    if (listen_res < 0)
    {
        printf("%s", strerror(errno));
        return 1;
    }

    // Accept next request
    struct sockaddr client_addr;
    socklen_t addrlen;
    // The file descriptor of the accepted socket
    int connfd = accept(sockfd, &client_addr, &addrlen);
    printf("Result accept : %d\n", connfd);
    if (connfd < 0)
    {
        printf("%s", strerror(errno));
        return 1;
    }

    // The buffer that receives the data
    char *buffer = (char *)malloc(sizeof(char) * 21);
    int nb_bytes_rcv = 1;
    int not_stop = 1;

    // Print the messages
    fflush(stdout);

    while (not_stop && nb_bytes_rcv != -1)
    {
        // Receive data from the accepted socket
        nb_bytes_rcv = read(connfd, buffer, 21);
        if (nb_bytes_rcv > 0)
        {
            printf("Number of bytes received : %d\n", nb_bytes_rcv);
            printf("%s\n", buffer);
        }

        // Check if the stop message was sent
        not_stop = strcmp(buffer, STOP);
    }

    // Free the allocated memory
    close(sockfd);
    free(buffer);
    return 0;
}