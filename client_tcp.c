#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <string.h>
#include <arpa/inet.h>
#include <errno.h>
#include <errno.h>
#include "utils.h"

#define SIZE 100
#define PORT 9600

int main(int argc, char *argv[])
{
    // The program doesn't run if too many arguments were given
    if (argc != 2)
    {
        printf("%s", "Invalid number of arguments\n");
        return 1;
    }

    // Declare sockfd
    int sockfd = socket(AF_INET, SOCK_STREAM, 0);

    // Declare the descriptive structure of the address
    struct sockaddr_in server_adress;
    server_adress.sin_family = AF_INET;
    server_adress.sin_port = PORT;
    inet_aton(argv[1], &server_adress.sin_addr);

    // Pointer to the descriptive structure of the server adress
    struct sockaddr *p_server_adress = (struct sockaddr *)&server_adress;
    int len_server_addr = sizeof(server_adress);

    // Send a request of connection to the server and wait the acceptance
    int connect_res = connect(sockfd, p_server_adress, len_server_addr);
    printf("Result connect : %d\n", connect_res);
    if (connect_res < 0)
    {
        printf("%s\n", strerror(errno));
        return 1;
    }

    // The connexion was succesful
    printf("Connexion success\n");

    // A message for user
    char *message = concat(concat("You can now send messages to ", argv[1]), ". Write ST0P to close the connection\n");
    printf("%s", message);
    // print the messages before we read the buffer
    fflush(stdout);

    // input buffer
    char *buffer = (char *)malloc(sizeof(char) * 21);
    // size of the sent string
    int size = 1;
    // loop condition, we stop if the message is "ST0P"
    int not_stop = 1;

    while (not_stop)
    {
        // read input
        size = read_input(buffer, 20);

        // check if the input is ST0P
        not_stop = strcmp(buffer, STOP);

        // send the string to the server and print the server's answewr
        int send_to_res = write(sockfd, buffer, size);
        printf("Bytes sent : %d\n", send_to_res);
        printf("%s\n", strerror(errno));
    }

    // Close the allocated memory
    free(buffer);
    close(sockfd);

    // End of the program
    return 0;
}