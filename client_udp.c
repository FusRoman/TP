#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <string.h>
#include <arpa/inet.h>
#include <errno.h>
#include "utils.h"

#define SIZE 100
#define PORT 9600

int main(int argc, char *argv[])
{
    // The program doesn't run if too many arguments were given
    if (argc != 2)
    {
        printf("%s", "Invalid number of arguments\n");
        return 1;
    }

    // Declare socket file description
    int sockfd = socket(AF_INET, SOCK_DGRAM, 0);

    // The descriptive structure of the local host
    struct sockaddr_in local_adress;
    local_adress.sin_family = AF_INET;
    local_adress.sin_port = PORT;
    local_adress.sin_addr.s_addr = INADDR_ANY;

    // Pointer to the descriptive structure of the host
    struct sockaddr *p_local_adress = (struct sockaddr *)&local_adress;
    int len_sock_addr = sizeof(local_adress);

    // Bind the socket file description to the local host
    int bind_res = bind(sockfd, p_local_adress, len_sock_addr);
    printf("Result bind : %d\n", bind_res);
    if (bind_res < 0)
    {
        printf("%s\n", strerror(errno));
        return 1;
    }

    // Descriptive structure of the server adress
    struct sockaddr_in server_adress;
    server_adress.sin_family = AF_INET;
    server_adress.sin_port = PORT;
    inet_aton(argv[1], &server_adress.sin_addr);

    // A message for the user
    // A message for user
    char *message = concat(concat("You can now send messages to ", argv[1]), ". Write ST0P to close the connection\n");
    printf("%s", message);
    fflush(stdout);

    // Input buffer
    char *buffer = (char *)malloc(sizeof(char) * 21);
    // size of the sent string
    int size = 0;
    // loop condition, we stop if the message is "ST0P"
    int not_stop = 1;
    // Result of the send
    int send_res = 0;
    while (not_stop && send_res > -1)
    {
        // read the input
        size = read_input(buffer, 20);

        // send the data
        send_res = sendto(sockfd, buffer, size, 0, (struct sockaddr *)&server_adress, sizeof(server_adress));
        printf("\nBytes send : %d\n", send_res);

        // check if the message is stop
        not_stop = strcmp(buffer, STOP);
    }

    // Free the allocated memory
    close(sockfd);
    free(buffer);
    return 0;
}