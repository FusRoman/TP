all:
	gcc -Wall -g utils.c -c -o utils.o
	gcc -Wall -g client_udp.c utils.o -o client_udp.out
	gcc -Wall -g server_udp.c utils.o -o server_udp.out
	gcc -Wall -g server_tcp.c utils.o -o server_tcp.out
	gcc -Wall -g client_tcp.c utils.o -o client_tcp.out

clean:
	rm -rf *.out
	rm -rf *.o